FROM devkitpro/devkitarm
ARG to_install="nano fish"
ENV INSTALL $to_install
COPY pass pass
RUN sudo apt update
RUN sudo apt upgrade -y
RUN sudo apt install curl build-essential git $INSTALL -y
RUN useradd --create-home dev
RUN usermod -aG sudo dev
RUN chpasswd < pass
RUN rm pass
RUN chown -R dev /home/dev/
RUN su - dev
ENV HOME /home/dev
#rustup install script
RUN curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain nightly -y
RUN ["/bin/bash", "-c", "source $HOME/.cargo/env", "&&", "rustup component add rust-src", "&&","cargo install xargo"]
RUN git clone https://github.com/rust3ds/rust3ds-template.git /home/dev/rust-3ds-template
USER dev
#WORKDIR /home/dev